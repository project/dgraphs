<?php
/**
 * @file
 * dgraphs_charts_demos.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function dgraphs_charts_demos_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'fish_production_import';
  $feeds_importer->config = array(
    'name' => 'Fish Production Import',
    'description' => 'Imports a data-set with fish produce in the great lakes.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => 0,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'public://feeds',
        'allowed_schemes' => array(),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'update_existing' => '1',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'Year',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Victoria',
            'target' => 'field_victoria',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Albert',
            'target' => 'field_albert',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Kyoga',
            'target' => 'field_kyoga',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'ED/GE',
            'target' => 'field_ed_ge',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Wamala',
            'target' => 'field_wamala',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'AL.NILE',
            'target' => 'field_al_nile',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'OTHERS',
            'target' => 'field_others',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Year',
            'target' => 'field_year',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '1',
        'authorize' => 0,
        'skip_hash_check' => 0,
        'bundle' => 'fish_production',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['fish_production_import'] = $feeds_importer;

  return $export;
}
