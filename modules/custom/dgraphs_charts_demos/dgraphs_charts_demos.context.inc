<?php
/**
 * @file
 * dgraphs_charts_demos.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function dgraphs_charts_demos_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'charts_graphs_bluff';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'charts_graphs_demo_bluff' => 'charts_graphs_demo_bluff',
        'charts_graphs_demo_bluff:page' => 'charts_graphs_demo_bluff:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-c3d4491351eece4718e0c4df1712b028' => array(
          'module' => 'views',
          'delta' => 'c3d4491351eece4718e0c4df1712b028',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-97fe162ebf8b10c5223e455339da66ae' => array(
          'module' => 'views',
          'delta' => '97fe162ebf8b10c5223e455339da66ae',
          'region' => 'content',
          'weight' => '2',
        ),
        'views-8571a458d5854d81c722d92f81bd5342' => array(
          'module' => 'views',
          'delta' => '8571a458d5854d81c722d92f81bd5342',
          'region' => 'content',
          'weight' => '3',
        ),
        'views-652cd2496bb6a0eb77b40afa3f5032cd' => array(
          'module' => 'views',
          'delta' => '652cd2496bb6a0eb77b40afa3f5032cd',
          'region' => 'content',
          'weight' => '4',
        ),
        'views-3788b875c62a4ae92b658f234ad980fa' => array(
          'module' => 'views',
          'delta' => '3788b875c62a4ae92b658f234ad980fa',
          'region' => 'content',
          'weight' => '5',
        ),
        'views-6b40b304e15a76ba256144a93fd03c2c' => array(
          'module' => 'views',
          'delta' => '6b40b304e15a76ba256144a93fd03c2c',
          'region' => 'content',
          'weight' => '6',
        ),
        'views-e8954275202a36d3cb5802edb483fab4' => array(
          'module' => 'views',
          'delta' => 'e8954275202a36d3cb5802edb483fab4',
          'region' => 'content',
          'weight' => '7',
        ),
        'views-548600174fb656999a2589f99bf35a41' => array(
          'module' => 'views',
          'delta' => '548600174fb656999a2589f99bf35a41',
          'region' => 'content',
          'weight' => '8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['charts_graphs_bluff'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'charts_graphs_d3';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'charts_graphs_demo' => 'charts_graphs_demo',
        'charts_graphs_demo:page' => 'charts_graphs_demo:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-charts_graphs_demo-block_4' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo-block_4',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-charts_graphs_demo-block_1' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo-block_1',
          'region' => 'content',
          'weight' => '11',
        ),
        'views-charts_graphs_demo-block_5' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo-block_5',
          'region' => 'content',
          'weight' => '12',
        ),
        'views-charts_graphs_demo-block_2' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo-block_2',
          'region' => 'content',
          'weight' => '13',
        ),
        'views-charts_graphs_demo-block_3' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo-block_3',
          'region' => 'content',
          'weight' => '14',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['charts_graphs_d3'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'charts_graphs_flot';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'charts_graphs_demo_flot' => 'charts_graphs_demo_flot',
        'charts_graphs_demo_flot:page' => 'charts_graphs_demo_flot:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-charts_graphs_demo_flot-block_2' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_2',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-charts_graphs_demo_flot-block_1' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_1',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-charts_graphs_demo_flot-block_3' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_3',
          'region' => 'content',
          'weight' => '2',
        ),
        'views-charts_graphs_demo_flot-block_4' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_4',
          'region' => 'content',
          'weight' => '3',
        ),
        'views-charts_graphs_demo_flot-block_5' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_5',
          'region' => 'content',
          'weight' => '4',
        ),
        'views-charts_graphs_demo_flot-block_6' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_6',
          'region' => 'content',
          'weight' => '5',
        ),
        'views-charts_graphs_demo_flot-block_7' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_7',
          'region' => 'content',
          'weight' => '6',
        ),
        'views-charts_graphs_demo_flot-block_8' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_8',
          'region' => 'content',
          'weight' => '7',
        ),
        'views-charts_graphs_demo_flot-block_9' => array(
          'module' => 'views',
          'delta' => 'charts_graphs_demo_flot-block_9',
          'region' => 'content',
          'weight' => '8',
        ),
        'views-f8e62d2c77bc392853ed3f133458bbaf' => array(
          'module' => 'views',
          'delta' => 'f8e62d2c77bc392853ed3f133458bbaf',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['charts_graphs_flot'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'charts_graphs_google';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'charts_graphs_demo_google' => 'charts_graphs_demo_google',
        'charts_graphs_demo_google:page' => 'charts_graphs_demo_google:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-f40854f0a93606d7415596bcf25005bc' => array(
          'module' => 'views',
          'delta' => 'f40854f0a93606d7415596bcf25005bc',
          'region' => 'content',
          'weight' => '1',
        ),
        'views-de2bf311d822bfb02c0b8de163dd4752' => array(
          'module' => 'views',
          'delta' => 'de2bf311d822bfb02c0b8de163dd4752',
          'region' => 'content',
          'weight' => '2',
        ),
        'views-9b54c5e3543c752945d05e55a5512f95' => array(
          'module' => 'views',
          'delta' => '9b54c5e3543c752945d05e55a5512f95',
          'region' => 'content',
          'weight' => '3',
        ),
        'views-d7a13e353aca1708fd9ee8b71361b00c' => array(
          'module' => 'views',
          'delta' => 'd7a13e353aca1708fd9ee8b71361b00c',
          'region' => 'content',
          'weight' => '4',
        ),
        'views-897587e8543b1e47345a2a9f36bf6745' => array(
          'module' => 'views',
          'delta' => '897587e8543b1e47345a2a9f36bf6745',
          'region' => 'content',
          'weight' => '5',
        ),
        'views-c3d96c82f6e785be7505aa188a856d31' => array(
          'module' => 'views',
          'delta' => 'c3d96c82f6e785be7505aa188a856d31',
          'region' => 'content',
          'weight' => '6',
        ),
        'views-5f505196d22ef928c361b33164eecab6' => array(
          'module' => 'views',
          'delta' => '5f505196d22ef928c361b33164eecab6',
          'region' => 'content',
          'weight' => '7',
        ),
        'views-dc1b584d9051402bf874405eae5a8407' => array(
          'module' => 'views',
          'delta' => 'dc1b584d9051402bf874405eae5a8407',
          'region' => 'content',
          'weight' => '8',
        ),
        'views-5277a1dcd7c10d9d7655dede94fc00f8' => array(
          'module' => 'views',
          'delta' => '5277a1dcd7c10d9d7655dede94fc00f8',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['charts_graphs_google'] = $context;

  return $export;
}
