<?php
/**
 * @file
 * dgraphs_charts_demos.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dgraphs_charts_demos_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:charts-graphs-buff
  $menu_links['main-menu:charts-graphs-buff'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'charts-graphs-buff',
    'router_path' => 'charts-graphs-buff',
    'link_title' => 'Bluff',
    'options' => array(),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
  );
  // Exported menu link: main-menu:charts-graphs-d3
  $menu_links['main-menu:charts-graphs-d3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'charts-graphs-d3',
    'router_path' => 'charts-graphs-d3',
    'link_title' => 'D3',
    'options' => array(),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
  );
  // Exported menu link: main-menu:charts-graphs-flot
  $menu_links['main-menu:charts-graphs-flot'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'charts-graphs-flot',
    'router_path' => 'charts-graphs-flot',
    'link_title' => 'Flot',
    'options' => array(),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
  );
  // Exported menu link: main-menu:charts-graphs-google
  $menu_links['main-menu:charts-graphs-google'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'charts-graphs-google',
    'router_path' => 'charts-graphs-google',
    'link_title' => 'Google',
    'options' => array(),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
  );
  // Exported menu link: main-menu:dgraphs-main
  $menu_links['main-menu:dgraphs-main'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'dgraphs-main',
    'router_path' => 'dgraphs-main',
    'link_title' => 'Dgraphs Imports',
    'options' => array(),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Bluff');
  t('D3');
  t('Dgraphs Imports');
  t('Flot');
  t('Google');


  return $menu_links;
}
