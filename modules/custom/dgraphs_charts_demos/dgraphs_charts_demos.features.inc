<?php
/**
 * @file
 * dgraphs_charts_demos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dgraphs_charts_demos_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dgraphs_charts_demos_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dgraphs_charts_demos_node_info() {
  $items = array(
    'fish_production' => array(
      'name' => t('Fish Production'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Year'),
      'help' => '',
    ),
  );
  return $items;
}
