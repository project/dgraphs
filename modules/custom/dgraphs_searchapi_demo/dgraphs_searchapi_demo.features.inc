<?php
/**
 * @file
 * dgraphs_searchapi_demo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dgraphs_searchapi_demo_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dgraphs_searchapi_demo_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function dgraphs_searchapi_demo_default_search_api_index() {
  $items = array();
  $items['users'] = entity_import('search_api_index', '{
    "name" : "users",
    "machine_name" : "users",
    "description" : null,
    "server" : "localhost",
    "item_type" : "user",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "uid" : { "type" : "integer" },
        "created" : { "type" : "date" },
        "field_gender" : { "type" : "string" },
        "field_admin_boundary" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "field_admin_boundary:field_location:wkt" : { "type" : "text" }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function dgraphs_searchapi_demo_default_search_api_server() {
  $items = array();
  $items['localhost'] = entity_import('search_api_server', '{
    "name" : "localhost",
    "machine_name" : "localhost",
    "description" : "",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids" : true,
      "scheme" : "http",
      "host" : "localhost",
      "port" : "8080",
      "path" : "\\/solr",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
