<?php
/**
 * @file
 * dgraphs_searchapi_demo.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function dgraphs_searchapi_demo_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-admin_boundaries-field_location'
  $field_instances['taxonomy_term-admin_boundaries-field_location'] = array(
    'bundle' => 'admin_boundaries',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'geofield',
        'settings' => array(
          'data' => 'full',
        ),
        'type' => 'geofield_wkt',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_location',
    'label' => 'Location',
    'required' => FALSE,
    'settings' => array(
      'local_solr' => array(
        'enabled' => FALSE,
        'lat_field' => 'lat',
        'lng_field' => 'lng',
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'geofield',
      'settings' => array(),
      'type' => 'geofield_openlayers',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');

  return $field_instances;
}
