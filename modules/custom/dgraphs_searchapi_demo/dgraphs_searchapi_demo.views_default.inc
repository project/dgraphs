<?php
/**
 * @file
 * dgraphs_searchapi_demo.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function dgraphs_searchapi_demo_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'test_users';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_users';
  $view->human_name = 'test_users';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'test_users';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Indexed User: User ID */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'search_api_index_users';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  /* Field: Indexed User: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'search_api_index_users';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['link_to_entity'] = 0;
  /* Field: Indexed User: Gender */
  $handler->display->display_options['fields']['field_gender']['id'] = 'field_gender';
  $handler->display->display_options['fields']['field_gender']['table'] = 'search_api_index_users';
  $handler->display->display_options['fields']['field_gender']['field'] = 'field_gender';
  /* Field: Admin Boundary » Location: Well-known text (indexed) */
  $handler->display->display_options['fields']['field_admin_boundary_field_location_wkt']['id'] = 'field_admin_boundary_field_location_wkt';
  $handler->display->display_options['fields']['field_admin_boundary_field_location_wkt']['table'] = 'search_api_index_users';
  $handler->display->display_options['fields']['field_admin_boundary_field_location_wkt']['field'] = 'field_admin_boundary_field_location_wkt';
  $handler->display->display_options['fields']['field_admin_boundary_field_location_wkt']['label'] = '';
  $handler->display->display_options['fields']['field_admin_boundary_field_location_wkt']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_admin_boundary_field_location_wkt']['link_to_entity'] = 0;

  /* Display: Users */
  $handler = $view->new_display('page', 'Users', 'page');
  $handler->display->display_options['path'] = 'test-users';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Users';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Mode */
  $handler = $view->new_display('mode', 'Mode', 'mode_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_map';
  $handler->display->display_options['style_options']['map'] = 'clone_of_default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
  );
  $handler->display->display_options['mode_id'] = 'map';
  $handler->display->display_options['mode_name'] = 'map';

  /* Display: OpenLayers Data Overlay */
  $handler = $view->new_display('openlayers', 'OpenLayers Data Overlay', 'openlayers_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'wkt',
    'other_lat' => 'uid',
    'other_lon' => 'uid',
    'wkt' => 'field_admin_boundary_field_location_wkt',
    'other_top' => 'uid',
    'other_right' => 'uid',
    'other_bottom' => 'uid',
    'other_left' => 'uid',
    'name_field' => 'uid',
    'description_field' => '',
    'style_field' => '',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['test_users'] = $view;

  return $export;
}
