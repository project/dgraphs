<?php
/**
 * @file
 * dgraphs_searchapi_demo.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function dgraphs_searchapi_demo_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'users';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'test_users' => 'test_users',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'facetapi_graphs-25FMdGQWUf1blRwxKjVmTOb7gJvs0N24' => array(
          'module' => 'facetapi_graphs',
          'delta' => '25FMdGQWUf1blRwxKjVmTOb7gJvs0N24',
          'region' => 'content',
          'weight' => '29',
        ),
        'facetapi_graphs-q8Od68Oe2NnEdxv5Pr15Cax09PUF0z0f' => array(
          'module' => 'facetapi_graphs',
          'delta' => 'q8Od68Oe2NnEdxv5Pr15Cax09PUF0z0f',
          'region' => 'content',
          'weight' => '30',
        ),
        'views_modes-modes' => array(
          'module' => 'views_modes',
          'delta' => 'modes',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'facetapi-2y0GQY9J2bu4OhVdPKqvtpILXpvF94bR' => array(
          'module' => 'facetapi',
          'delta' => '2y0GQY9J2bu4OhVdPKqvtpILXpvF94bR',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'facetapi-S1r0uPHZJmPLXf8NRM0f3K3jCYjigk3F' => array(
          'module' => 'facetapi',
          'delta' => 'S1r0uPHZJmPLXf8NRM0f3K3jCYjigk3F',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['users'] = $context;

  return $export;
}
