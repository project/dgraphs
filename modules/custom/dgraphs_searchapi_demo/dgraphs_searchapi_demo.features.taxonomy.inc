<?php
/**
 * @file
 * dgraphs_searchapi_demo.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function dgraphs_searchapi_demo_taxonomy_default_vocabularies() {
  return array(
    'admin_boundaries' => array(
      'name' => 'Admin Boundaries',
      'machine_name' => 'admin_boundaries',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
