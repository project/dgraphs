<?php
/**
 * @file
 * dgraphs_searchapi_demo.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function dgraphs_searchapi_demo_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_location'
  $field_bases['field_location'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_location',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'geofield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'geofield',
  );

  return $field_bases;
}
