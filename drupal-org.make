; dgraphs make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[context][version] = 3
projects[context][type] = "module"
projects[context][subdir] = "contrib"

projects[ctools][version] = 1.3
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[entity][version] = 1.1
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[views][version] = 3.7
projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[views_infinite_scroll][version] = 1.1
projects[views_infinite_scroll][type] = "module"
projects[views_infinite_scroll][subdir] = "contrib"

projects[features][subdir] = contrib
projects[features][type] = "module"
projects[features][version] = 2.0-rc1

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[libraries][version] = 2.1
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

; Other contribs.

projects[advanced_help][version] = 1.x-dev
projects[advanced_help][type] = "module"
projects[advanced_help][subdir] = "contrib"

projects[token][version] = 1.5
projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[token][patch][] = "http://drupal.org/files/token-token_asort_tokens-1712336_0.patch"

projects[date][version] = 2.6
projects[date][type] = "module"
projects[date][subdir] = "contrib"

;Search related modules

projects[search_api][subdir] = contrib
projects[search_api][type] = "module"
projects[search_api][version] = 1.6

projects[search_api_db][version] = 1.0-rc1
projects[search_api_db][type] = "module"
projects[search_api_db][subdir] = "contrib"

projects[search_api_solr][version] = 1.2
projects[search_api_solr][type] = "module"
projects[search_api_solr][subdir] = "contrib"

projects[search_api_solr_overrides][version] = 1.0-rc1
projects[search_api_solr_overrides][type] = "module"
projects[search_api_solr_overrides][subdir] = "contrib"

projects[facetapi][version] = 1.3
projects[facetapi][type] = "module"
projects[facetapi][subdir] = "patched"
projects[facetapi][patch][] = "http://drupal.org/files/facetapi-1616518-13-show-active-term.patch"
projects[facetapi][patch][] = "http://drupal.org/files/1665164-facetapi-override_facet_label-7.patch"

projects[search_api_sorts][version] = 1.4
projects[search_api_sorts][type] = "module"
projects[search_api_sorts][subdir] = "contrib"

projects[search_api_swatches][version] = 1
projects[search_api_swatches][type] = "module"
projects[search_api_swatches][subdir] = "contrib"
projects[search_api_swatches][download][type] = git
projects[search_api_swatches][download][url] = "http://git.drupal.org/project/search_api_swatches.git"

projects[purl][version] = 1.x-dev
projects[purl][type] = "module"
projects[purl][subdir] = "contrib"

projects[purl_search_api][version] = 1.0-beta1
projects[purl_search_api][type] = "module"
projects[purl_search_api][subdir] = "contrib"

projects[views_modes][version] = 1.x-dev
projects[views_modes][type] = "module"
projects[views_modes][subdir] = "contrib"

; Graphing

projects[charts_graphs][version] = 2.0
projects[charts_graphs][type] = "module"
projects[charts_graphs][subdir] = "contrib"

projects[views_charts][version] = 1.x-dev
projects[views_charts][type] = "module"
projects[views_charts][subdir] = "contrib"

projects[charts_graphs_flot][version] = 1.x-dev
projects[charts_graphs_flot][type] = "module"
projects[charts_graphs_flot][subdir] = "contrib"

projects[facetapi_graphs][version] = 1.x-dev
projects[facetapi_graphs][type] = "module"
projects[facetapi_graphs][subdir] = "contrib"

projects[flot][version] = 1.x-dev
projects[flot][type] = "module"
projects[flot][subdir] = "contrib"

projects[d3][version] = 1.x-dev
projects[d3][type] = "module"
projects[d3][subdir] = "contrib"

projects[charts_graphs_d3][version] = 1.x-dev
projects[charts_graphs_d3][type] = "module"
projects[charts_graphs_d3][subdir] = "contrib"
projects[charts_graphs_d3][download][type] = git
projects[charts_graphs_d3][download][url] = "http://git.drupal.org/sandbox/ssekono/2028575.git"

projects[d3_charts][version] = 1.x-dev
projects[d3_charts][type] = "module"
projects[d3_charts][subdir] = "contrib"
projects[d3_charts][download][type] = git
projects[d3_charts][download][url] = http://git.drupal.org/sandbox/ssekono/2075121.git

projects[charts_and_graphs_bluff][version] = 1.x-dev
projects[charts_and_graphs_bluff][type] = "module"
projects[charts_and_graphs_bluff][subdir] = "contrib"
projects[charts_and_graphs_bluff][download][type] = git
projects[charts_and_graphs_bluff][download][url] = "http://git.drupal.org/sandbox/dmulindwa/1973492.git"

projects[charts_graphs_google_charts][version] = 1.x-dev
projects[charts_graphs_google_charts][type] = "module"
projects[charts_graphs_google_charts][subdir] = "contrib"
projects[charts_graphs_google_charts][download][type] = git
projects[charts_graphs_google_charts][download][url] = "http://git.drupal.org/sandbox/dmulindwa/1973720.git"

; ++++ mapping ++++

projects[geofield][version] = 2.0
projects[geofield][type] = "module"
projects[geofield][subdir] = "contrib"

projects[openlayers][version] = 2.0-beta7
projects[openlayers][type] = "module"
projects[openlayers][subdir] = "contrib"

projects[geophp][version] = 1.7
projects[geophp][type] = "module"
projects[geophp][subdir] = "contrib"

projects[proj4js][version] = 1.2
projects[proj4js][type] = "module"
projects[proj4js][subdir] = "contrib"

; Development
projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][type] = "module"
projects[job_scheduler][subdir] = "contrib"

projects[feeds][version] = 2.0-alpha8
projects[feeds][type] = "module"
projects[feeds][subdir] = "contrib"

; Quick stuff
projects[admin_menu][version] = 3
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[diff][version] = 3
projects[diff][type] = "module"
projects[diff][subdir] = "contrib"

projects[devel][version] = 1.3
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

; +++++ Libraries +++++

; flot
libraries[flot][download][type] = "file"
libraries[flot][download][url] = "http://flot.googlecode.com/files/flot-0.7.tar.gz"
libraries[flot][download][sha1] = "68ca8b250d18203ebe67136913e0e1c82bbeecfb"
libraries[flot][destination] = "libraries"

; d3
libraries[d3][download][type] = "get"
libraries[d3][download][url] = "http://github.com/mbostock/d3/zipball/master"
libraries[d3][directory_name] = "d3"
libraries[d3][destination] = "libraries"

